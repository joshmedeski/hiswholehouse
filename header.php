<?php
/**
 * Header
 *
 * Setup the header for our theme
 *
 * @package WordPress
 * @subpackage Foundation, for WordPress
 * @since Foundation, for WordPress 1.0
 */
?>

<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-s-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" />
<link href='http://fonts.googleapis.com/css?family=Oleo+Script' rel='stylesheet' type='text/css'>
<!-- Set the viewport width to device width for mobile -->
<meta name="viewport" content="width=device-width" />
 <title><?php if ( is_home() ) {
	echo('A Home for Healing');
} else {
	wp_title('');
} ?> | His Whole House</title>

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<!--Header-->
<header id="top">
	<!--Logo-->
	<div class="row" style="padding: 15px 0;">
		<div class="four columns"><a href="/"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="The Whole House Logo"></a>
		</div>
		
		<div id="header-right" style="padding-top: 40px;">
			<div class="five columns hide-for-small" style="text-align: right;">
				<a href="https://www.facebook.com/hiswholehouse" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/facebook.png" alt=""></a>
				<a href="http://www.youtube.com/user/TheWholeHouseMcM" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/youtube.png" alt=""></a>
				<a href="http://feeds.feedburner.com/HisWholeHouse" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/rss.png" alt=""></a>
			</div>
			<div class="three columns" id="search">
			<?php get_search_form( ); ?>
			</div>
		</div>
	</div>
</header>
		
<nav id="menu">
<div class="row">
	<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'menu_class' => 'nav-bar', 'fallback_cb' => 'foundation_page_menu', 'container' => 'nav', 'container_class' => 'twelve columns', 'walker' => new foundation_navigation() ) ); ?>
	</div>
</nav>

<!-- Begin Page -->
<div class="textured">
<div class="row">
<div class="twelve columns">

<?php if (is_home()) : ?>
	<div id="featuredContent" class="hide-for-small orbit">
  <div>
    <div class="six columns">
	    <a href="about"><img src="<?php bloginfo('template_directory'); ?>/images/welcome.jpg" alt="Molly and the Dogs"></a>
    </div>
    <div class="six columns">
	    <h1>Welcome</h1>
	    <p>“…in whom the whole building, being fitly joined together, grows into a holy temple in the Lord. In whom you also are being built together for a habitation of God in the Spirit” - Ephesians 2:22-23</p>
    </div>
  </div>
  <div>
	<div class="six columns">
	    <h1>Prayer Ministry</h1>
	    <p>Enable yourself to be set free and restored in areas of your heart to the original purpose for which you were created.</p>
	</div>
	<div class="six columns">
	    <a href="prayer"><img src="<?php bloginfo('template_directory'); ?>/images/prayer.jpg" alt="Praying hands in front of flames"></a>
	</div>
  </div>
  <div>
   	    <div class="six columns">
	    <a href="/training-and-equipping/trauma-train-the-trainer/"><img src="<?php bloginfo('template_directory'); ?>/images/trauma.png" alt="Molly and the Dogs"></a>
    </div>
    <div class="six columns">
    	<h1>Upcoming Events</h1>
    	<?php if (class_exists('EM_Events')) { echo EM_Events::output( array('limit'=>3,'orderby'=>'event_start_date') );
} ?>

    </div>
  </div>
  <div>
   	    <div class="six columns">
	   	    <h1>Topic of the month</h1>
	   	    <p>Each month a special topic is discussed by the Whole House team.</p>
	   	    <a href="category/topic-of-the-month" class="button">Read</a>
	   	    </div> 
	   	<div class="six columns">
	   	<a href="category/topic-of-the-month"><img src="<?php bloginfo('template_directory'); ?>/images/topic.jpg" alt="door"></a>
	   	</div>  	
  </div>
	</div>
	<h1 class="show-for-small">Welcome</h1>
<?php else : ?>
    <h1><?php wp_title(''); ?></h1>
<?php endif; ?>



</div></div></div>
    <div id="page-content">
    <div class="row">