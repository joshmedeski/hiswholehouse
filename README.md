## Credits
This site was built by [Josh Medeski](http://medeskidesign.com/) for [His Whole House](http://hiswholehouse.net) using [Zurb's Foundation](http://foundation.zurb.com/) with help from [Drewsymo's Starter Theme](https://github.com/drewsymo/Foundation).

### Design Inspiration
There is nothing new under the sun. Here are website the team took inspiration from while working on this project.

- [islandchurchgalveston](http://www.islandchurchgalveston.com/)
- [heatherbrown](http://heatherbrown.us/)
- [lamppostcreative](http://lamppostcreative.com/)
- [ministriesofpastoralcare](http://ministriesofpastoralcare.com/)
- [findinghomeinstitute](http://findinghomeinstitute.org/)
- [googleanalytics](http://www.google.com/analytics/)
- [dawntodawn](http://dawntodawn.org/)



## License
This theme was built for "His Whole House".