<?php 
/**
 * The template for displaying Search Results.
 */
 get_header(); ?>

<!-- Main Content -->
<div class="nine columns">

		<?php if ( have_posts() ) : ?>
			
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/* Include the post format-specific template for the content. If you want to
				 * this in a child theme then include a file called called content-___.php
				 * (where ___ is the post format) and that will be used instead.
				 */
				get_template_part( 'content', get_post_format() );

			endwhile;
			?>

		<?php else : ?>
			<?php get_template_part( 'content', 'foundation' ); ?>
		<?php endif; ?>


</div>
<!-- End Main Content -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>