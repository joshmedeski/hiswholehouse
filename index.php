<?php
/**
 * Index
 *
 * Standard loop for the front-page
 *
 * @package WordPress
 * @subpackage Foundation, for WordPress
 * @since Foundation, for WordPress 1.0
 */

get_header(); ?>
<div class="six columns">
	<h2>Recent Articles</h2><hr>
	<?php if ( have_posts() ) : ?>
	<?php query_posts('posts_per_page=3&cat=-5,-69'); ?>

		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content', get_post_format() ); ?>
		<?php endwhile; ?>

	<?php else : ?>
	<?php endif; ?>	
</div>
    
<div class="six columns">
	<h2>Recent Videos</h2><hr>
	<?php if ( have_posts() ) : ?>
	<?php query_posts('posts_per_page=1&cat=5'); ?>

		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content-featuredvideo', get_post_format() ); ?>
		<?php endwhile; ?>

	<?php else : ?>
	<?php endif; ?>	
	
	<?php if ( have_posts() ) : ?>
	<?php query_posts('posts_per_page=2&cat=5&offset=1'); ?>

		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content', get_post_format() ); ?>
		<?php endwhile; ?>

	<?php else : ?>
	<?php endif; ?>
</div>
<?php get_footer(); ?>