<?php
/**
 * Content Link
 *
 * Displays content shown in the 'index.php' loop, default for 'standard' post format
 *
 * @package WordPress
 * @subpackage Foundation, for WordPress
 * @since Foundation, for WordPress 1.0
 */
?>
<article class="twelve columns">
			<h3><a href="<?php

  $post_link = get_post_custom_values('wp_post_link');
  foreach ( $post_link as $key => $value ) {
    echo "$value"; 
  }

?>" title="<?php echo esc_attr( sprintf( __( 'Link to %s', 'foundation' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark" target="_blank"><?php the_title(); ?></a></h3>
</article>


