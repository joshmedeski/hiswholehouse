<?php
/**
 * Content
 *
 * Displays content shown in the 'index.php' loop, default for 'standard' post format
 *
 * @package WordPress
 * @subpackage Foundation, for WordPress
 * @since Foundation, for WordPress 1.0
 */
?>
<article class="twelve columns">
	<div class="row">
	<div class="twelve columns">
	<hgroup>
			<h3><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'foundation' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
			<h6><?php the_time(get_option('date_format')); ?> in 
			<?php 
				$categories = get_the_category();
				$separator = ' , ';
				$output = '';
				if($categories){
				foreach($categories as $category) {
				$output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
				}
				echo trim($output, $separator);
				}
			?> by <?php the_author(); ?></h6>
		</hgroup>
	</div>
	</div>
	
	<div class="row">	
		<div class="twelve columns">		
			<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
			<?php the_excerpt(); ?>
		</div>


	
	</div>

</article>