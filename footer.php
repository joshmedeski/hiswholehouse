<?php
/**
 * Footer
 *
 * Displays content shown in the footer section
 *
 * @package WordPress
 * @subpackage Foundation, for WordPress
 * @since Foundation, for WordPress 1.0
 */
?>

</div><!-- End Row -->
</div> <!-- End Content Div -->
<!-- End Page -->

<!-- Footer -->
<footer id="bottom"> 
  <div class="row">

  <?php if ( dynamic_sidebar('Sidebar Footer One') && dynamic_sidebar('Sidebar Footer Two') && dynamic_sidebar('Sidebar Footer Three') && dynamic_sidebar('Sidebar Footer Four')  ) : else : ?>

  <div class="twelve columns">
  	<ul class="link-list">
  		<?php wp_list_bookmarks('categorize=0&title_li='); ?>
  	</ul>
  </div>

  <?php endif; ?>

  </div>
</footer>

<div id="copyright">
  <div class="row">
  	<div class="five columns">
  		<p>&copy; Copyright 2013 His Whole House</p>
  	</div>
  	<div class="offset-by-two five columns text-right">
    	<p>Designed by <a href="http://medeskidesign.com/" target="_blank" style="text-decoration: none; color: #7a7979">Josh Medeski</a></p>
  	</div>
  </div>
</div>
<!-- End Footer -->
<?php wp_footer(); ?>
<script type='text/javascript'>
   
   $(window).load(function() {
       $('#featured').orbit();
       $('#featuredContent').orbit({ 
        fluid: '16x6', 
        bullets: true, 
        animation: 'horizontal-push',
        timer: true,
        resetTimerOnClick: false,
        advanceSpeed: 5000,
        directionalNav: true,
        });
   });
</script>
</body>
</html>