<?php
/**
 * Content Single
 *
 * Loop content in single post template (single.php)
 *
 * @package WordPress
 * @subpackage Foundation, for WordPress
 * @since Foundation, for WordPress 1.0
 */
?>

<article>
	<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
	<?php the_content(); ?>
	<hr>
	<footer>
		<p><em>Did this inspire, challenge, or move you?</em> <a href="mailto:contact@hiswholehouse.net">Email us</a></p>
		<p>
			Posted on <?php the_time(get_option('date_format')); ?> in <?php the_category(', '); ?> by <?php the_author(); ?> <?php if (in_category('Stories to Live by')) : ?>of Newman Ministries<?php endif; ?> <a href="<?php the_author_url(); ?>" target="_blank">(Learn more)</a>.<br>
			<?php the_tags(); ?>
		</p>
	</footer>
</article>