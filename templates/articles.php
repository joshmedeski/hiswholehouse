<?php
/*
 * Template Name: Articles Template
 */

get_header(); ?>
    <!-- Main Content -->
    <div class="nine columns" role="content">

		<?php if ( have_posts() ) : ?>
		<?php query_posts('posts_per_page=5'. '&cat=-4,-69'.'&paged='.$paged); ?>

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

		<?php else : ?>
			
		<?php endif; ?>
   <div class="row">
   <div class="twelve columns">
	<?php foundation_pagination(); ?>
   </div></div>
    </div>
    <!-- End Main Content -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
