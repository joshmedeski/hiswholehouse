<?php
/*
 * Category Name: Videos
 */

get_header(); ?>
    <!-- Main Content -->
<div class="nine columns">

    <!-- Main Content -->
	<?php $i = 1; ?>
		<?php echo '<div class="row"><div class="twelve columns">'; ?>
		<?php if ( $wp_query->have_posts() ) : ?>

		<?php while ( $wp_query->have_posts() ) : ?> 
		<?php $wp_query->the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>

		<?php if($i % 2 == 0) {echo '</div></div><hr><div class="row"><div class="twelve columns">';} ?>
	
	<?php $i++; endwhile; endif;
	echo '</div></div>'; ?>			

    <div class="row">
    <div class="twelve columns">
		<?php foundation_pagination(); ?>
    </div></div>

</div>
    <!-- End Main Content -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
