<?php
/*
 * Category Name: Topic of the Month
 * Displays current topic + archive in footer
 */

get_header(); ?>
    <!-- Main Content -->
<div class="nine columns">
<!-- Start Article -->
<?php $my_query = new WP_Query('category_name=topic-of-the-month&showposts=1'); ?>
<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
<h2 style="font-size: 30px;"><strong><?php the_title(); ?></strong></h2>
	<?php the_content(); ?>
<?php endwhile; ?>
<!-- End Article -->
<div class="row">
<div class="twelve columns">
<hr>
<h3>Archive</h3>
<p><ul>
<?php query_posts('category_name=topic-of-the-month&showpost'); ?>
<?php while (have_posts()) : the_post(); ?>
        <li><?php the_time('F, Y') ?>: <a href="<?php the_permalink(); ?>">
          <?php the_title(); ?>  
          </a>  </li>
        <?php endwhile; ?>
</ul></p></div></div>


</div>
    <!-- End Main Content -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
