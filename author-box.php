<?php
/**
 * Author Box
 *
 * Displays author box with author description and thumbnail on single posts
 *
 * @package WordPress
 * @subpackage Foundation, for WordPress
 * @since Foundation, for WordPress 1.0
 */
?>

<section class="row">
	<div class="twelve columns">

		<div class="author-box">
			<h4><?php _e('About', 'foundation' ); ?> <?php the_author(); ?></h4>
			<p style="text-align:justify;">
				<span class="th"><?php echo get_avatar( get_the_author_meta('user_email'),'150' ); ?></span>
				<?php echo get_the_author_meta('description'); ?>
			</p>
		</div>
		
	</div>
</section>