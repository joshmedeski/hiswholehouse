<?php
/**
 * 404
 *
 * In case something goes wrong
 *
 * @package WordPress
 * @subpackage Foundation, for WordPress
 * @since Foundation, for WordPress 1.0
 */

get_header(); ?>
    <div class="twelve columns">
			<h2 style="text-align:center;padding: 100px 0;">Oops... Nothing's here. Do you need <a href="http://hiswholehouse.net/prayer/">prayer?</a></h2>  	
    </div>
<?php get_footer(); ?>